FROM openjdk:12
ADD target/docker-spring-boot-gabriel_federizzi.jar docker-spring-boot-gabriel_federizzi.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-gabriel_federizzi.jar"]
